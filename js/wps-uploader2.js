jQuery(document).ready(function($) {
    $('#upload_logo_button').click(function() {
        tb_show('Choose a logo', 'media-upload.php?referer=wp-starters&type=image&TB_iframe=1&post_id=0', false);
        //return false;
        
        window.send_to_editor = function(html) {
    	    var logo_url = $('img',html).attr('src');	    
            var logo_width = $('img',html).attr('width');
            var logo_height = $('img',html).attr('height');       
    	    var logo_url_array = logo_url.split('/');
    	    var logo_name = logo_url_array[logo_url.length-1];
    	    var logo_ratio = Math.min( 253 / logo_width, 66 / logo_height );
    	    var logo_scaled_width = Math.floor( logo_ratio * logo_width );
    	    var logo_scaled_height = Math.floor( logo_ratio * logo_height );
    	    
    	    $('#wps_logo_hidden').val(logo_url);
    	    $('#wps_logo_width_hidden').val(logo_scaled_width);
    	    $('#wps_logo_height_hidden').val(logo_scaled_height);
    	    $('#wps_logo_label').text(logo_url);
    	    $('#wps_logo_preview').attr('src',logo_url);
    	    $('#wps_logo_preview').attr('width',logo_scaled_width);
    	    $('#wps_logo_preview').attr('height',logo_scaled_height);
    	    $('#submit_options_form').trigger('click');
    	    tb_remove();
    	}
    });
    
    $('#wps_upload_bg_button').click(function() {
        tb_show('Choose a background', 'media-upload.php?referer=wp-starters&type=image&TB_iframe=1&post_id=0', false);
        //return false;
        
        window.send_to_editor = function(html) {
    	    var bg_url = $('img',html).attr('src');	    
            var bg_width = $('img',html).attr('width');
            var bg_height = $('img',html).attr('height');       
    	    var bg_url_array = bg_url.split('/');
    	    var bg_name = bg_url_array[bg_url.length-1];
    	    var bg_ratio = Math.min( 400 / bg_width, 300 / bg_height );
    	    var bg_scaled_width = Math.floor( bg_ratio * bg_width );
    	    var bg_scaled_height = Math.floor( bg_ratio * bg_height );
    	    
    	    $('#wps_bg_hidden').val(bg_url);
    	    $('#wps_bg_label').text(bg_url);
    	    $('#wps_bg_preview').attr('src', bg_url);
    	    $('#wps_bg_preview').attr('width', bg_scaled_width);    
    	    $('#wps_bg_preview').attr('height',bg_scaled_height);
    	    $('#submit_options_form').trigger('click');
    	    tb_remove();
    	}
    });
});
