jQuery(document).ready(function($){


    var logo_selector;
    var bg_selector;


    $('#wps_upload_logo_button').click(function(e) {

        e.preventDefault();

        if (logo_selector) {
            logo_selector.open();
            return;
        }

        logo_selector = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            library : {
                type : 'image'
            },            
            multiple: false,
        });

        logo_selector.on('select', function() {
            attachment = logo_selector.state().get('selection').first().toJSON();
            var logo_width = attachment.width;
            var logo_height = attachment.height; 
            var logo_ratio = Math.min( 253 / logo_width, 66 / logo_height );
            var logo_scaled_width = Math.floor( logo_ratio * logo_width );
    	    var logo_scaled_height = Math.floor( logo_ratio * logo_height );
            
    	    $('#wps_logo_preview').attr('src',attachment.url);
            $('#wps_logo_preview').attr('width',logo_scaled_width);
    	    $('#wps_logo_preview').attr('height',logo_scaled_height);
    	    $('#wps_logo_hidden').val(attachment.url);
            $('#wps_logo_width_hidden').attr('width',logo_scaled_width);
    	    $('#wps_logo_height_hidden').attr('height',logo_scaled_height);
    	    $('#wps_logo_width_hidden').val(logo_scaled_width);
    	    $('#wps_logo_height_hidden').val(logo_scaled_height);
    	    $('#wps_logo_label').text(attachment.filename);
        });

        logo_selector.open();

    });
    
    $('#wps_upload_bg_button').click(function(e) {

        e.preventDefault();

        if (bg_selector) {
            bg_selector.open();
            return;
        }

        bg_selector = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            library : {
                type : 'image'
            },            
            multiple: false,
        });

        bg_selector.on('select', function() {
            attachment = bg_selector.state().get('selection').first().toJSON();
            //var bg_width = attachment.width;
            //var bg_height = attachment.height; 
            //var bg_ratio = Math.min( 253 / bg_width, 66 / bg_height );
            //var bg_scaled_width = Math.floor( bg_ratio * bg_width );
    	    //var bg_scaled_height = Math.floor( bg_ratio * bg_height );
            
    	    $('#wps_bg_preview').attr('src',attachment.url);
            //$('#wps_bg_preview').attr('width',bg_scaled_width);
    	    //$('#wps_bg_preview').attr('height',bg_scaled_height);
    	    $('#wps_bg_hidden').val(attachment.url);
            //$('#wps_bg_width_hidden').attr('width',bg_scaled_width);
    	    //$('#wps_bg_height_hidden').attr('height',bg_scaled_height);
    	    //$('#wps_bg_width_hidden').val(bg_scaled_width);
    	    //$('#wps_bg_height_hidden').val(bg_scaled_height);
    	    $('#wps_bg_label').text(attachment.filename);
        });

        bg_selector.open();

    });
}); 
