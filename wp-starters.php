<?php
/**
 * Plugin Name: WP Starters
 * Plugin URI: http://www.netpad.gr
 * Description: Removes the default WordPress logo and link from wp-login
 * Version: 1.0
 * Author: George Tsiokos
 * Author URI: http://www.netpad.gr
 * License: GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *	
 * Copyright (c) 2015-2016 "gtsiokos" George Tsiokos www.netpad.gr
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as 
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 
 *  
 */
// @todo check readme.txt file
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

define( 'WP_WPS_DIR', 		plugin_dir_path( __FILE__ ) );
define( 'WP_WPS_URL', 		plugin_dir_url( __FILE__ ) );
define( 'WP_WPS_FILE', 		__FILE__ );
define( 'WP_WPS_DIR_ADMIN', WP_WPS_DIR . 'admin/' );
define( 'WP_WPS_DIR_CORE', 	WP_WPS_DIR . 'core/' );
define( 'WP_WPS_DIR_JS', 	'/wp-content/plugins/wp-starters/js/' );
define( 'WP_WPS_DIR_CSS', 	'/wp-content/plugins/wp-starters/css/' );

require_once( WP_WPS_DIR_ADMIN . 	'class-wps-admin.php' );
require_once( WP_WPS_DIR_CORE . 	'class-wp-starters.php' );
?>
