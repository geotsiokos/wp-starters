<?php
class WP_Starters {

	const PLUGIN_OPTIONS = 'wp_starters';
	const WPS_LOGO_URL = 'wps_logo_url';
	const WPS_BG_URL = 'wps_bg_url';
	const WPS_LOGO_WIDTH = 'wps_logo_width';
	const WPS_LOGO_HEIGHT = 'wps_logo_height';
	const WPS_DEFAULT_LOGO = 'wps_default_logo';
	const WPS_DEFAULT_BG = 'wps_default_bg';
	const WPS_BG_COLOR = 'wps_bg_color';
	const WPS_MAIL_OPTIONS = 'wps_mail_options';
	const WPS_DATA_DELETE = 'wps_data_delete';
	const DEFAULT_STRING = '';
	const WPS_NONCE = 'wp_starters_nonce';
	const WPS_ADMIN_OPTIONS = 'set_admin_options';
	
	public static function init() {
		add_action( 'login_enqueue_scripts',  	array( __CLASS__, 'wps_login_logo' ) );
		add_filter( 'login_headerurl', 			array( __CLASS__, 'wps_logo_link' ) );
		add_filter( 'login_headertitle', 		array( __CLASS__, 'wps_logo_description' ) );
		add_filter( 'wp_mail_from', 			array( __CLASS__, 'wps_mail_address') );
		add_filter( 'wp_mail_from_name', 		array( __CLASS__, 'wps_mail_name') );
		register_deactivation_hook( WP_WPS_FILE, 	array( __CLASS__, 'wps_deactivate' ) );
		register_uninstall_hook( WP_WPS_FILE, 	array( __CLASS__, 'wps_uninstall' ) );
	}
	
	/**
	 * Change wp-login logo
	 *
	 */
	public static function wps_login_logo() {
		$output = '<style type="text/css">';
		$options = get_option( self::PLUGIN_OPTIONS , array() );
		$wps_default_logo = isset( $options[ self::WPS_DEFAULT_LOGO ] ) ? $options[ self::WPS_DEFAULT_LOGO ] : '';
		$wps_default_bg = isset( $options[ self::WPS_DEFAULT_BG ] ) ? $options[ self::WPS_DEFAULT_BG ] : '';
		// Logo
		if ( $wps_default_logo == '' ) {		
			if ( isset( $options[ self::WPS_LOGO_URL ] ) && $options[ self::WPS_LOGO_URL ] != '' ) {
				
				$wps_logo = $options[ self::WPS_LOGO_URL ];
				$wps_logo_width = isset( $options[ self::WPS_LOGO_WIDTH ] ) ? $options[ self::WPS_LOGO_WIDTH ] : '';
    			$wps_logo_height = isset( $options[ self::WPS_LOGO_HEIGHT ] ) ? $options[ self::WPS_LOGO_HEIGHT ] : '';
				
				$output .= 'body.login div#login h1 a {';
				$output .= '	background-image: url("'.$wps_logo.'");';
				$output .= '	background-size: '.$wps_logo_width.'px '.$wps_logo_height.'px;';
				$output .= '	width: '.$wps_logo_width.'px;';
				$output .= '	height: '.$wps_logo_height.'px;';
				$output .= '}';				
			
			} else {
				$output .= '';
			}
		}	
		
		// Background
		//@todo seperate color from bg 
		if ( $wps_default_bg == '' ) {
			if ( isset( $options[ self::WPS_BG_URL ] ) && $options[ self::WPS_BG_URL ] != '' ) {
		
				$wps_bg = $options[ self::WPS_BG_URL ];
				$wps_color = $options[ self::WPS_BG_COLOR ];
		
				$output .= 'body {';
				$output .= '	background-image: url("'.$wps_bg.'") !important;';
				$output .= '	background-color: #'.$wps_color.' !important;';
				$output .= '}';
				$output .= '.login #backtoblog, .login #nav {';
				$output .= '	background-color: #f1f1f1;';
				$output .= '}';
			} else {
				$output .= '';
			}
		}
		$output .= '</style>';
		
		echo $output;
	}
	    
	/**
	 * Change the logo link
	 * @return string
	 */
    public static function wps_logo_link( $default_link ) {
    	$options = get_option( self::PLUGIN_OPTIONS , array() );
    	$wps_default_logo = isset( $options[ self::WPS_DEFAULT_LOGO ] ) ? $options[ self::WPS_DEFAULT_LOGO ] : '';
    	$wps_logo_link = $default_link; 
    	if ( $wps_default_logo == '' ) {
    		$wps_logo_link = home_url();
    	}
    	
        return $wps_logo_link;
    }
    
    /**
     * Change the logo tooltip on mouse hover
     * @return string
     */
    public static function wps_logo_description() {
        return get_bloginfo( 'description' );
    }
    
    /**
     * Change the 'from' email address in mail messages
     * @param unknown_type $default_address
     * @return string
     */
    public static function wps_mail_address( $default_address ) {
    	$options = get_option( self::PLUGIN_OPTIONS , array() );
    	$wps_mail_options = isset( $options[ self::WPS_MAIL_OPTIONS ] ) ? $options[ self::WPS_MAIL_OPTIONS ] : '';
    	$wps_mail_address = $default_address;
    	if ( $wps_mail_options != '' ) {
    		$wps_mail_address = get_bloginfo( 'admin_email' );
    	}
    	return $wps_mail_address;
    }
    
    /**
     * Change the 'from' name in mail messages
     * @param string $default_name
     * @return string
     */
    public static function wps_mail_name( $default_name ) {
    	$options = get_option( self::PLUGIN_OPTIONS , array() );
    	$wps_mail_options = isset( $options[ self::WPS_MAIL_OPTIONS ] ) ? $options[ self::WPS_MAIL_OPTIONS ] : '';
    	$wps_mail_from_name = $default_name;
    	if ( $wps_mail_options != '' ) {
    		$wps_mail_from_name = get_bloginfo( 'name' );
    	}
    	return $wps_mail_from_name;
    }
    
    /**
     * Clean options table upon plugin deactivate
     */
    public static function wps_deactivate() {
    	$options = (array) get_option( self::PLUGIN_OPTIONS );
    	$wps_data_delete = isset( $options[ self::WPS_DATA_DELETE ] ) ? $options[ self::WPS_DATA_DELETE ] : '';
    	if ( $wps_data_delete != '' ) {
    		delete_option ( 'wp_starters' );
    	}
    }
    
    /**
     * Clean options table upon plugin uninstall
     */
    public function wps_uninstall() {
    	delete_option ( 'wp_starters' );
    }
    
} WP_Starters::init();