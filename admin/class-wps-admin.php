<?php
if ( !defined('ABSPATH' ) ) {
	exit;
}
// @todo modify readme.txt add new features tags etc...
// @todo add assets folder to keep css js images etc...
// @todo remove bg option for pro and add color option
class WP_Starters_Admin {
	
	const PLUGIN_OPTIONS = 'wp_starters';
	const WPS_LOGO_URL = 'wps_logo_url';
	const WPS_BG_URL = 'wps_bg_url';
	const WPS_LOGO_WIDTH = 'wps_logo_width';
	const WPS_LOGO_HEIGHT = 'wps_logo_height';
	const WPS_DEFAULT_LOGO = 'wps_default_logo';
	const WPS_DEFAULT_BG = 'wps_default_bg';
	const WPS_BG_COLOR = 'wps_bg_color';
	const WPS_MAIL_OPTIONS = 'wps_mail_options';
	const WPS_DATA_DELETE = 'wps_data_delete';
	const DEFAULT_STRING = '';
	const WPS_NONCE = 'wp_starters_nonce';
	const WPS_ADMIN_OPTIONS = 'set_admin_options';
	
	public function __construct() {
		add_action( 'admin_menu', 				array( $this, 'wps_admin_menu' ) );
		add_action( 'admin_init', 				array( $this, 'wps_admin_init' ) );
		add_action( 'admin_enqueue_scripts', 	array( $this, 'wps_admin_enqueue_scripts' ) );	
	}			
		
	/**
	 * The admin options page
	 * 
	 */ 
	public function wps_admin_menu() {
		add_submenu_page( 
				'options-general.php',
				'WP Starters Options',
				'WP Starters',
				'manage_options',
				'wp-starters',
				array( $this, 'wps_settings' )
		);
	}
	
	
	public function wps_settings()	{		
        echo '<div class="wrap">';
        echo '<h2>WP Starters Settings</h2>';            
        echo '<form method="post" action="options.php">';
        	settings_fields( 'wps_settings' );   
        	do_settings_sections( 'wp-starters' );
        	echo '<hr>';
        	submit_button( 'Save' );
        echo '</form>';
        echo '</div>';
    }
    
    public function wps_admin_init() {  
    	register_setting ( 'wps_settings', self::PLUGIN_OPTIONS, 		array( $this, 'wps_settings_validation' ) );
    	// Logo
    	add_settings_section( 'wps_settings_logo_section', 'Choose a logo for wp-login.php', array( $this, 'wps_logo_section' ), 'wp-starters' );    	    	
    	add_settings_field( 'wps_upload_logo_button', 'Upload or choose from library', array( $this, 'wps_select_logo' ), 'wp-starters', 'wps_settings_logo_section' );
    	add_settings_field( 'wps_default_logo', 'Use default logo', array( $this, 'wps_default_logo_settings' ), 'wp-starters', 'wps_settings_logo_section' );
    	/// Background Image
    	add_settings_section( 'wps_settings_bg_section', 'Choose a background color for wp-login.php', array( $this, 'wps_bg_section' ), 'wp-starters' );    	
    	//add_settings_field( 'wps_upload_bg_button', 'Upload or choose from library', array( $this, 'wps_select_bg' ), 'wp-starters', 'wps_settings_bg_section' );    	
    	 // Background Color
    	add_settings_field( 'wps_bg_color', 'Background Color', array( $this, 'wps_select_color' ), 'wp-starters', 'wps_settings_bg_section' );
    	add_settings_field( 'wps_default_bg', 'Use default color', array( $this, 'wps_default_bg_settings' ), 'wp-starters', 'wps_settings_bg_section' );
    	//add_settings_field( 'wps_default_bg', 'Use default background', array( $this, 'wps_default_bg_settings' ), 'wp-starters', 'wps_settings_bg_section' );
    	// Email message from and name options
    	add_settings_section( 'wps_mail_settings_section', 'E-mail Settings', array( $this, 'wps_mail_section' ), 'wp-starters' );
    	add_settings_field( 'wps_mail_settings', 'Enable E-mail Settings', array( $this, 'wps_mail_settings' ), 'wp-starters', 'wps_mail_settings_section' );
    	
    	// Data persistence
    	add_settings_section( 'wps_settings_data_delete_section', 'Delete plugin data on deactivation', array( $this, 'wps_data_delete_section' ), 'wp-starters' );
    	add_settings_field( 'wps_data_delete', 'Delete data', array( $this, 'wps_data_delete' ), 'wp-starters', 'wps_settings_data_delete_section' );
    }
    
    /**
     * Validates data.   
     *
     * @param array $input 
     * @return array
     */ 
    public function wps_settings_validation( $input ) {
    	
    	if ( isset( $input[ self::WPS_LOGO_URL ] ) && $input[ self::WPS_LOGO_URL ] != '' ) {
    		$valid_input[ self::WPS_LOGO_URL ] = $input[ self::WPS_LOGO_URL ];
    		if ( ! self::wps_check_image_format( $input[ self::WPS_LOGO_URL ] ) ) {
    			add_settings_error( 'wps_logo_format_error', 'wps_logo_format_error', 'The file format for your logo image is not valid. Please choose an image in jpg, gif or png format.', 'error' );
    			$valid_input[ self::WPS_LOGO_URL ] = '';
    		} 
    	} else {
    		$valid_input[ self::WPS_LOGO_URL ] = '';
    		if ( !isset( $input[ self::WPS_DEFAULT_LOGO ] ) ) {
    			add_settings_error( 'wps_logo_empty_error', 'wps_logo_empty_error', 'Please choose a logo image in jpg, gif or png format, or check the <i>Default</i> option.', 'error' );
    		}
    	}
    	// Logo dimensions
    	if ( !isset( $input[ self::WPS_LOGO_WIDTH ] ) ) {
    		list( $width, $height ) = getimagesize( $input[ self::WPS_LOGO_URL ] );
    		$valid_input[ self::WPS_LOGO_WIDTH ] = intval( $width );
    	} else {
    		$valid_input[ self::WPS_LOGO_WIDTH ] = $input[ self::WPS_LOGO_WIDTH ];
    	}
    	if ( $input[ self::WPS_LOGO_HEIGHT ] == '' ) {
    		list( $width, $height ) = getimagesize( $input[ self::WPS_LOGO_URL ] );
    		$valid_input[ self::WPS_LOGO_HEIGHT ] = intval( $height );
    	} else {
    		$valid_input[ self::WPS_LOGO_HEIGHT ] = $input[ self::WPS_LOGO_HEIGHT ];
    	}
    	
    	// Background Color
    	if ( isset( $input[ self::WPS_BG_COLOR ] ) ) {
    		// @todo remove '#'
    		if ( strpos( $input[ self::WPS_BG_COLOR ], '#' ) != false ) {
    			write_log( 'found' );
    			$input[ self::WPS_BG_COLOR ] = substr( $input[ self::WPS_BG_COLOR ], 1 );
    		}    		
    		if ( preg_match( '/([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})/', $input[ self::WPS_BG_COLOR ] ) ) {
    			$valid_input[ self::WPS_BG_COLOR ] = $input[ self::WPS_BG_COLOR ];
    		} else {
    			$valid_input[ self::WPS_BG_COLOR ] = '';
    			$input[ self::WPS_DEFAULT_BG ] = 'on';
    			add_settings_error( 'wps_bg_color_error', 'wps_bg_color_error', 'The color code is not valid. ', 'error' );
    		}
    	}
    	// Background image
    	/*if ( isset( $input[ self::WPS_BG_URL ] ) && $input[ self::WPS_BG_URL ] != '' ) {
    		$valid_input[ self::WPS_BG_URL ] = $input[ self::WPS_BG_URL ];
    		if ( ! self::wps_check_image_format( $input[ self::WPS_BG_URL ] ) ) {
    			add_settings_error( 'wps_bg_format_error', 'wps_bg_format_error', 'The file format for your background image is not valid. Please choose an image in jpg, gif or png format.', 'error' );
    			$valid_input[ self::WPS_BG_URL ] = '';
    		}  	
    	} else {
    		$valid_input[ self::WPS_BG_URL ] = '';
    		if ( !isset( $input[ self::WPS_DEFAULT_BG ] ) ) {
    			add_settings_error( 'wps_bg_empty_error', 'wps_bg_empty_error', 'Please choose a background image in jpg, gif or png format, or check the <i>Default</i> option.', 'error' );
    		}
    	} */	
    	//write_log( 'default color' );
    	//write_log( $input[ self::WPS_DEFAULT_BG ] );
    	$valid_input[ self::WPS_DEFAULT_LOGO ] = isset( $input[ self::WPS_DEFAULT_LOGO ] ) ? $input[ self::WPS_DEFAULT_LOGO ] : '';
    	$valid_input[ self::WPS_DEFAULT_BG ] = isset( $input[ self::WPS_DEFAULT_BG ] ) ? $input[ self::WPS_DEFAULT_BG ] : '';
    	$valid_input[ self::WPS_MAIL_OPTIONS ] = isset( $input[ self::WPS_MAIL_OPTIONS ] ) ? $input[ self::WPS_MAIL_OPTIONS ] : '';
    	$valid_input[ self::WPS_DATA_DELETE ] = isset( $input[ self::WPS_DATA_DELETE ] ) ? $input[ self::WPS_DATA_DELETE ] : '';
    	
	    return $valid_input;
    }   
    
    /**
     * 	Prints some tips about the logo image to be uploaded/used
     */
    public function wps_logo_section() {
    	echo 'You can choose your image from media library, or upload a new image.<br />'; 
    	echo 'Maximum supported dimensions are 253 x 66 px, so your image will be resized proportionally.<br /> ';
    	echo 'i.e. if your chosen image has a resolution of 800 x 600 px, it will be scaled down to 88 x 66 px.';
    }
    
    public function wps_bg_section() {
    	echo 'Enter the hexadecimal color code.';
    }
    
    public function wps_mail_section() {
   		echo 'Enable this option to change WordPress default email <i>From</i> name and <i>address</i> to the admin <strong>Email Address</strong> and <strong>Site Title</strong> respectively.';
    }
    
    public function wps_data_delete_section() {
    	echo 'If you select this option, all saved settings will be deleted once the plugin is deactivated.';
    	echo '<br />Once deleted, data cannot be recovered, so you should use it with caution.';
    }
        
    /**
     * Wp-login logo image select form
     */
    public function wps_select_logo() {      	
    	$options = (array) get_option( self::PLUGIN_OPTIONS );
    	$wps_logo = isset( $options[ self::WPS_LOGO_URL ] ) ? $options[ self::WPS_LOGO_URL ] : '';
    	$wps_default_logo = isset( $options[ self::WPS_DEFAULT_LOGO ] ) ? $options[ self::WPS_DEFAULT_LOGO ] : '';
    	$wps_logo_width = 1;
    	$wps_logo_height = 1;

    	if ( $wps_logo != '' ) {
	    	if ( !isset( $options[ self::WPS_LOGO_WIDTH ] ) || !isset( $options[ self::WPS_LOGO_HEIGHT ] ) ) {
	    		$img_dimensions = self::wps_image_resize( $wps_logo );
	    		$wps_logo_width = $img_dimensions[0];
	    		$wps_logo_height = $img_dimensions[1];
	    	} else {
	    		$wps_logo_width = $options[ self::WPS_LOGO_WIDTH ];
	    		$wps_logo_height = $options[ self::WPS_LOGO_HEIGHT ];
	    	} 
    	}    	
    	
    	echo	'<input id="wps_upload_logo_button" type="button" class="button" value="Choose your logo..." />';
    	echo	'<input id="wps_logo_hidden" name="wp_starters[' . self::WPS_LOGO_URL . ']" type="hidden" value="'.$wps_logo.'" />';
    	echo	'<input id="wps_logo_width_hidden" name="wp_starters[' . self::WPS_LOGO_WIDTH . ']" type="hidden" value="'.$wps_logo_width.'" />';
    	echo	'<input id="wps_logo_height_hidden" name="wp_starters[' . self::WPS_LOGO_HEIGHT . ']" type="hidden" value="'.$wps_logo_height.'" />';
    	echo	'<p><i>Your logo will look like this in wp-login</i></p>';
    	echo	'<p><img id="wps_logo_preview" src="'.$wps_logo.'" width="'.$wps_logo_width.'" height="'.$wps_logo_height.'" /></p>';
    	echo	'<label id="wps_logo_label" class="wps_logo_label">&nbsp</label>';
    }
    
    /**
     * Background color field
     */
    public function wps_select_color() {
    	$options = (array) get_option( self::PLUGIN_OPTIONS );
    	$wps_bg_color = isset( $options[ self::WPS_BG_COLOR ] ) ? $options[ self::WPS_BG_COLOR ] : '';
    	
    	echo	'Enter the hexadecimal code for your color';
    	echo	'#<input id="wps_bg_color" type="text" class="wps_bg_color" name="wp_starters['. self::WPS_BG_COLOR .']" value="'.$wps_bg_color.'" />';
    }
    
    /**
     * 	Background image select form
     */
    /*public function wps_select_bg() {
    	$options = (array) get_option( self::PLUGIN_OPTIONS );
    	$wps_bg_url = isset( $options[ self::WPS_BG_URL ] ) ? $options[ self::WPS_BG_URL ] : '';
    	$wps_bg_width = 400;
    	if ( $wps_bg_url == '' ) {
    		$wps_bg_width = 1;
    		$wps_bg_height = 1;
    	}
    	
    	echo	'<input id="wps_upload_bg_button" type="button" class="button" value="Choose your background..." />';
    	echo	'<input id="wps_bg_hidden" name="wp_starters[wps_bg_url]" type="hidden" value="'.$wps_bg_url.'" />';
    	echo	'<label id="wps_bg_label" class="wps_bg_label">&nbsp</label>';
    	echo	'<p><i>The background image you have chosen scaled down to 400 x 300 px to fit screen</i></p>';
    	echo	'<p><img id="wps_bg_preview" src="'.$wps_bg_url.'" width="'.$wps_bg_width.'" height="" /></p>';
    } */       
    
    /**
     * 	Default logo option
     */
    public function wps_default_logo_settings() {
    	$options = (array) get_option( self::PLUGIN_OPTIONS );
    	$wps_default_logo = isset( $options[ self::WPS_DEFAULT_LOGO ] ) ? $options[ self::WPS_DEFAULT_LOGO ] : '';    	
    	echo	'<input id="wps_default_logo" name="wp_starters[' . self::WPS_DEFAULT_LOGO . ']" type="checkbox" '. ( $wps_default_logo ? ' checked="checked" ' : '' ) .' />';    	
    }
    
    /**
     * 	Default background option
     */
    public function wps_default_bg_settings() {
    	$options = (array) get_option( self::PLUGIN_OPTIONS );
    	$wps_default_bg = isset( $options[ self::WPS_DEFAULT_BG ] ) ? $options[ self::WPS_DEFAULT_BG ] : '';
    	echo	'<input id="wps_default_bg" name="wp_starters[' . self::WPS_DEFAULT_BG . ']" type="checkbox" '. ( $wps_default_bg ? ' checked="checked" ' : '' ) .' />';
    }
    
    /**
     * Mail sender field option
     */    
    public function wps_mail_settings() {
    	$options = (array) get_option( self::PLUGIN_OPTIONS );
    	$wps_mail_options = isset( $options[ self::WPS_MAIL_OPTIONS ] ) ? $options[ self::WPS_MAIL_OPTIONS ] : '';
    	
    	echo	'<input id="wps_mail_from" name="wp_starters[' . self::WPS_MAIL_OPTIONS . ']" type="checkbox" '. ( $wps_mail_options ? ' checked="checked" ' : '' ) .' />';
    }
    
    /**
     * Data persistence upon plugin deactivation
     */
    public function wps_data_delete() {
    	$options = (array) get_option( self::PLUGIN_OPTIONS );
    	$wps_data_delete = isset( $options[ self::WPS_DATA_DELETE ] ) ? $options[ self::WPS_DATA_DELETE ] : '';
    	
    	echo	'<input id="wps_data_delete" name="wp_starters[' . self::WPS_DATA_DELETE . ']" type="checkbox" '. ( $wps_data_delete ? ' checked="checked" ' : '' ) .' />';
    }
    
    /**
     * Load libraries and styles
     */
    public function wps_admin_enqueue_scripts() {
    	$screen = get_current_screen();
    	if ( isset( $screen->id ) && 'settings_page_wp-starters' == $screen->id ) {
    		wp_enqueue_media();
    		wp_register_script('wps_uploader', WP_WPS_DIR_JS . 'wps_uploader.js', array( 'jquery') );
    		wp_enqueue_script('wps_uploader');    		
    		wp_register_style( 'wp-starters', WP_WPS_DIR_CSS . 'wp-starters.css' );
    		wp_enqueue_style( 'wp-starters' ); 
	    	//wp_enqueue_script( 'jquery' );    
    	}
    }    
       
    /**
     * Image resize helper function 
     * @param string $wps_img_url
     * @return array
     */
    public function wps_image_resize( $wps_img_url ) {
    	$wps_img_res = false;
    	
    	if ( getimagesize( $wps_img_url ) != false ) {
    		list( $width, $height ) = getimagesize( $wps_img_url );
    		$wps_img_ratio = min( ( 253 / $width ), ( 66 / $height) );
    		$wps_img_res[0] = floor( $width * $wps_img_ratio );
    		$wps_img_res[1] = floor( $height * $wps_img_ratio );
    	}
    	
    	return $wps_img_res;
    }        
    
    /**
     * Checks image format helper function
     * @param string
     * @return boolean
     */
    public static function wps_check_image_format( $image_url ) {
    	$wps_path_parts = pathinfo( $image_url, PATHINFO_EXTENSION );
    	$valid_image = true;
    	 
    	if ( !in_array( $wps_path_parts, array( 'jpg', 'jpeg', 'png', 'gif') ) ) {
    		$valid_image = false;
    	}
    	 
    	return $valid_image;
    }
    
} 
if( is_admin() ) {
	$wp_starters_admin = new WP_Starters_Admin();
}
?>