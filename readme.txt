=== WP Starters ===
Contributors: gtsiokos
Tags: wp-login, logo, home_url, background, e-mail
Donate link: www.netpad.gr
Requires at least: 4.4
Tested up to: 4.6.1
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Replace the default WP logo, set a background image and change the from address and name for all WP e-mail messages.

== Description ==
You can replace the default WordPress logo and choose another image from media library or upload a new image. The chosen image will be resized automatically and link to the front page of your site.
You can replace "WordPress" from the "from" name for all your outgoing emails. 

== Installation ==
1. Extract the wp-starters/ folder file to /wp-content/plugins/
2. Activate the plugin at your blog\'s Dashboard -> Plugins screen

== Frequently Asked Questions ==

= What does this plugin do? =

It helps you replace default logo and set a background image for wp-login.
You can also change the from email address and name for all the WP mails sent by your site.

= How can i configure the plugin

The plugin settings can be found at your blog\'s Dashboard -> Settings -> WP Starters

== Screenshots ==

1. Logo - this is where you set a new logo for your wp_login page.
2. Background - here you can set an image to be used for background in you wp_login page.
3. Mail settngs - by enabling this you can change your from address and name fields, on the emails sent from your installation.

== Changelog ==

= 1.0 =
* This is the initial version of WP Starters plugin.

== Upgrade Notice ==
= 1.0 =
This is the initial version of WP Starters plugin.

== Important Notes ==
* This plugin has not been tested with versions of WordPress prior to 4.0.
* Do not use this plugin if you are using another plugin to manage wp-login.php. It has not been tested with that setup.

### Feedback ###

Feedback is welcome!

If you need help, have problems, want to leave feedback or want to provide constructive criticism, please do so here at the [WP Starters plugin page](http://www.netpad.gr/category/wordpress/plugins).

Please try to solve problems there before you rate this plugin or say it doesn't work. There goes a _lot_ of work into providing you with free quality plugins! Please appreciate that and help with your feedback. 
Thanks!

#### Twitter ####

[Follow @tsiokosg on Twitter](http://twitter.com/tsiokosg) for updates on this plugin.
